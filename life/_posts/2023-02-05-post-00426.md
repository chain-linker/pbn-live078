---
layout: post
title: "Must Read Classics 069: 백년의 고독, 가브리엘 가르시아 마르케스"
toc: true
---


## Must Read Classics 069: 백년의 고독, 가브리엘 가르시아 마르케스
 

### 《고전:백년의 고독, One Hundred Years of Solitude》 유개 요약
 One Hundred Years of Solitude is a novel by the Colombian author Gabriel García Márquez, published in 1967. The book tells the story of the Buendía family over the course of several generations in the fictional town of Macondo. The family patriarch, José Arcadio Buendía, founds the town with his wife Úrsula Iguarán and begins a lineage that is plagued by both external and internal conflicts.
 백년간의 고독은 1967년에 출간된 콜롬비아 문예가 가브리엘 가르시아 마르케스의 소설입니다. 이 책은 가상의 부서 마콘도에서 여러 세대에 걸쳐 펼쳐지는 부엔디아 가족의 이야기를 담고 있습니다. 가문의 족장인 호세 아르카디오 부엔디아는 각시 우르술라 이과란과 나란히 마을을 세우고 외부와 내부의 갈등에 시달리는 혈통을 이어나가기 시작합니다.

 The novel is set in a magical realist world, where supernatural events and everyday reality coexist. The Buendía family is cursed by the past and the mistakes of their ancestors, leading to recurring themes of incest, madness, and loneliness. The story is also heavily influenced by Colombian history and culture, with references to real-life events such as the Banana Massacre and the Thousand Days War.
 이 소설은 초자연적인 사건과 일상적인 현실이 공존하는 마술적 사실주의 세계를 배경으로 합니다. 부엔디아 가문은 과거와 조상들의 실수로 저주를 받아 근친상간, 광기, 외로움이라는 주제들이 반복적으로 발생합니다. 뿐만 아니라 바나나 대학살과 천일전쟁과 같은 실제 사건을 언급하는 등 콜롬비아의 역사와 문화에 큰 영향을 받은 이야기입니다.
 

 As the novel progresses, each member of the Buendía family faces their own struggles and tragedies, from José Arcadio Buendía's obsession with alchemy to Aureliano Buendía's involvement in political rebellion. Despite the family's efforts to break the cycle of their history, they are unable to escape their fate.
 소설이 진행됨에 따라 호세 아르카디오 부엔디아의 연금술에 대한 집착부터 아우렐리아노 부엔디아가 정치적 반란에 연루되는 등 부엔디아 가족 구성원 각자가 각자의 고난과 비극에 직면하게 됩니다. 역사의 굴레를 끊으려는 가족들의 노력에도 불구하고 그들은 운명을 피할 수 없습니다.
 

 The novel also explores themes of love, passion, and sexuality, with several characters engaging in forbidden romances and enduring unrequited love. The book's rich symbolism [리니지 프리서버](https://screwslippery.com/life/post-00036.html) and magical realism have made it a classic of Latin American literature, and it has been translated into numerous languages and adapted into film, theater, and other media.
 이 소설은 또한 사랑, 열정, 섹슈얼리티라는 주제를 탐구하며 여러 등장인물이 금지된 로맨스에 참여하고 짝사랑을 견뎌냅니다. 이 소설은 풍부한 상징성과 마법 같은 사실주의로 라틴 아메리카 문학의 고전이 되었으며, 수많은 언어로 번역되어 영화, 연극 및 기타 미디어로 각색되었습니다.
 

 Ultimately, One Hundred Years of Solitude is a complex exploration of the human experience, delving into the depths of human emotion and history in a way that is both beautiful and haunting.
 궁극적으로 '백 년의 고독'은 인간 경험에 대한 복합적인 탐험으로, 아름답고 잊혀지지 않는 방식으로 인간의 감정과 역사의 깊이를 파고듭니다.

### 《고전: 백년의 고독》에 나오는 멋진 문구/글귀
 “Many years later, as he faced the firing squad, Colonel Aureliano Buendía was to remember that distant afternoon when his father took him to discover ice.”
 "수년 후 총살대에 서게 된 아우렐리아노 부엔디아 대령은 아버지와 함께 얼음을 발견하러 갔던 그 먼 오후를 기억했습니다."
 

 “He was still too young to know that the heart's memory eliminates the bad and magnifies the good, and that thanks to this artifice we manage to endure the burden of the past.”
 "그는 아직 너무 어려서 마음의 기억이 나쁜 것을 제거하고 좋은 것을 확대한다는 사실과 이 기억 덕분에 과거의 짐을 견뎌낼 수 있다는 사실을 알지 못했습니다."
 

 “There is always something left to love.”
 "사랑할 것은 항상 남아 있습니다."
 

 “He dugs so deeply into her sentiments that in search of interest he found love, because by trying to make her love him he ended up falling in love with her.”
 "그는 그녀의 감정에 너무 깊이 파고 들어 관심을 찾아 사랑을 찾았고, 그녀가 그를 사랑하게 만들려고 노력함으로써 결국 그녀와 사랑에 빠졌기 때문입니다."
 

 “The secret of a good old age is simply an honorable pact with solitude.”
 "좋은 노년의 비결은 단순히 고독과의 명예로운 계약입니다."
 

 “It is not true that people stop pursuing dreams because they grow old, they grow old because they stop pursuing dreams.”
 "나이가 들기 때문에 꿈을 좇는 것을 멈추는 것이 아니라 꿈을 좇지 않기 때문에 늙는다는 것은 사실이 아니다."
 

 “Then he made one last effort to search in his heart for the place where his affection had rotted away, and he could not find it.” "그런 다음 그는 애정이 썩어버린 곳을 마음속에서 찾기 위해 마지막으로 노력했지만 찾을 수 없었습니다."
 

 “He was overwhelmed by the feeling that everything he had ever done in his life had been futile.”
 "그는 자신이 지금까지 해왔던 모든 일이 부질없었다는 생각에 압도당했습니다."
 

 “It was the moment when solitude ceased to be solitude, and became loneliness.”
 "고독이 고독이 아닌 외로움이 되는 순간이었습니다."
 

 “She would defend herself, saying that love, no matter what else it might be, was a natural talent. She had a native feel for it. Some people had a feel for navigation, or for flower arranging. Elizabeth had a feel for love.”
 "그녀는 사랑은 다른 어떤 것이든 간에 타고난 재능이라고 말하며 자신을 변호하곤 했습니다. 그녀는 타고난 감각이 있었죠. 어떤 사람들은 내비게이션이나 꽃꽂이에 재능이 있죠. 엘리자베스는 사랑에 대한 감각이 있었습니다."
