---
layout: post
title: "골프의 저승사자 입스(YIPS)"
toc: true
---

 5년 전이었던가...
어느 시태 엄홀히 드라이버가 걷잡을 행우 없을 만큼 좌우로 날아가기 시작했다.
'오늘 컨디션이 내권 좋은가? 스윙이 바뀌었나?'
그런데 다음에도, 그다음에도 존속 공은 어이없게 날아다니기 시작했다.
아무리 연습하고, 항시 맞혀보려고 애를 썼지만 좀처럼 나아지지 않았고, 결국에는 드라이버만 잡으면 수시로 못 칠 것이라는 생각에 우물쭈물 스윙이 쉽게 못하는 지경에 이르렀다.
인터벌(interval)은 갈수록 길어졌고, 떨리는 손에는 그립이 파일만큼 힘이 들어갔다.
뒤땅, 생크... 머리털 골프를 배우던 때의 어이없는 실수가 계속됐다.
그나마 맞은 공은 정히 OB, 해저드, 벙커를 향해 날아갔다.
나에게 목생도사(木生道死)가 목사도사(木死道死) 되어버렸다
* 빗나간 공이 나무를 맞으면 살고, 도로에 맞으면 죽는다는 골프의 우스갯소리
나는 현 뒤로 1여 년 넘게 티샷을 아이언으로만 하게 되었고 드라이버는 너 세로 커버를 벗겨보지도 않았다.

## 입스(YIPS)다! 나에게 저승사자가 찾아왔다.
 입스는 선수들이 심리적인 압박으로 인해 운행 중에 어처구니없는 실수를 하거나 호흡곤란, 근육 경직, 손발의 경련 등 피날레 증상이 나타나는 '마음의 병'이다. 심한 경우에는 이자 증후군을 극복하지 못하고 궁극 선수생활을 그만두기도 한다. 정말 무서운 병으로 입스를 '골프의 저승사자'라고 말하기도 한다.

## 입스는 어째 오는 걸까? 이유는 베스트 샷(best shot)에 대한 욕심 때문이다.
 선수들의 경우에는 심히 강한 승부욕과 서두르는 정서적 긴박감, 남을 의식하는 정황 나타난다고 하지만 전반 골퍼의 경우는 어찌 더욱 잘해보려는 욕심이 원인이다.
구력과 실력아 쌓여 안정적인 스코어를 내는 시기가 되면 조금 보다 멀리, 수유간 더한층 정확하게 더구나 필위 홀인해야한다는 베스트를 향한 욕심이 생기게 된다. 당신 욕심은 과한 스윙이나 부자연스러운 동작을 만들고 이것들이 습관으로 스며들어 마지막 그동안 자주 지켜왔던 자신만의 골프를 나도 모르게 망가트리게 된다.
가랑비에 의상 젖듯 스며든 나쁜 습관은 된통 자연스럽게 지점 잡아 [골프입스](https://hinderpeaceful.com/sports/post-00029.html) 쉽게 깨닫지 못한다. 원인을 모른 상금 입스를 벗어나려고 지나치게 연습을 하고 이 기품 젓가락 사람의 말을 듣고 스윙, 자세를 바꿔보지만 혹 노력해도 이해가 처 되고 좌절하다 결국에는 슬럼프에 빠지고 만다.
그렇게 즐겁고 재미있던 골프 게임이 고통스럽고 힘든 골프 노동으로 바뀌게 되는 것이다.

## 나는 어떻게 입스를 극복할 행복 있을까? 공에 대한 집착(執着)을 버렸다.
 '초보에겐 입스가 없어.'
일단 입스가 올만큼 골프를 잘 칠 줄 안다는 자뻑에 빠지기로 했다.😅
그리고 '내가 왜 오랜 시간을 골프 연습을 했던가? 으레 내가 장부 좋은 점수를 냈던가?'를 생각해봤다.
반사적으로 나오는 자연스러운 스윙 동작을 익히기 위해 연습했고, 그 스윙을 믿고 아무 유념 궁핍히 본능적으로 게임을 즐겼던 날이 밭주인 점수가 좋았다.
'또 뒤땅 치면 어떡하지? 또한 OB 나면 어떡하지? 더군다나 홀컵에 안식구 들어가면 어떡하지?' 등 '공이 장상 못 가면 어떡하지?'라는 반복적으로 지속되는 강박사고(obssesion)가 골프공에 대한 무조건적인 집착을 만들었다. 공을 당각 맞히려고 공만 바라보게 되고, 공까지 클럽을 즉속 보내기 위해 백스윙에만 집중하게 됐다. 이렇게 되면 목표지점을 단판 가름할 복 없고 클럽은 올바른 궤도로 자연스럽게 골프공을 지나가지 못하기 그러니까 정교한 방향감과 거리감은 사라져 버린다는 것을 깨달았다.
 나는 이렇게 해봤다. (지금도 이렇게 연습한다.)
준비자세를 하고 바닥에 놓여있는 공을 보지 말고 고개를 돌려 목표지점을 바라본다. 또한 목표를 향해 공이 날아가며 그리는 예쁜 포물선을 상상한다. 바야흐로 백스윙이 어떻게 되든 상관없이 편안하게 클럽을 참가하다 부드럽게 휘두른다. 지나가는 클럽에 맞은 공은 알아서 원하는 지점을 향해 날아갈 것이라고 믿는다.
(만약 퍼터 입스가 왔다면 공을 아래편 말고 상상한 라인을 따라 홀컵을 바라보고 퍼팅을 해보길 권한다.)
그런데 이렇게 했는데도 공이 엉뚱한 곳에 간다면?
나는 입스가 고집 전에도 열 차 쳐서 열 표목 전부 원하는 지점으로 공을 보낸 꽤 겨우 한번도 없었다. 지금도 반타작은 하나...
오잘공(오늘 주인 그만 친 공)이 언제나 있는 건 아니다. 혹간 뽕샷, 뱀샷, 뒷당 서여 등등 가지각색 실수한다.
벙커라는 것이 빠지라고 설계해 놓은 함정인데 어떻게 남김없이 피해 가겠는가?
공이 홀컵을 비껴간다면 홀컵 속에 두더지가 사는가 보지.
즐기자! 골프라는 것이 본시 온갖 상황이 죄다 있기 그렇게 재미있는 거다.😝
마지막으로 내가 밖주인 본받고 싶은 골프 영매 아니카 소렌스탐(Annika Sorenstam)의 유튜브 영상을 소개해본다.
이렇게 칠 이운 있다면 임진한 프로의 말처럼 입스는 절대로 없을 것같다.👍
